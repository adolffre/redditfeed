//
//  FAUCommentsViewController.h
//  RedditFeed
//
//  Created by Adolf Jurgens on 07/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FAUPost.h"
#import "FAUWebViewController.h"

@interface FAUCommentsViewController : UIViewController<UIScrollViewDelegate, UIGestureRecognizerDelegate>
@property (strong, nonatomic)  UIScrollView *pScrollGeral;

@property (strong, nonatomic) FAUPost *pSelectedPost;
@property (weak, nonatomic) IBOutlet UILabel *pAuthorPost;
@property (weak, nonatomic) IBOutlet UILabel *pSubDomainPost;
@property (weak, nonatomic) IBOutlet UILabel *pTextPost;
@property (weak, nonatomic) IBOutlet UILabel *pDomainCommentsPost;
@property (weak, nonatomic) IBOutlet UIImageView *pThumbnailPost;
@property (strong, nonatomic) NSMutableArray *pScrollArray;
@property (strong, nonatomic) NSMutableArray *pVisiblePosts;
@property (strong, nonatomic)  FAUWebViewController *FAUWebVC;


@end

