//
//  FAUPost.h
//  RedditFeed
//
//  Created by Adolf Jurgens on 05/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FAUPost : NSObject
@property (strong, nonatomic) NSDecimalNumber *pNumComments;
@property (strong, nonatomic) NSDecimalNumber *pScore;
@property (strong, nonatomic) NSString *pDomain;
@property (strong, nonatomic) NSString *pIdPost;
@property (strong, nonatomic) NSString *pSelfText;
@property (strong, nonatomic) NSString *pSubReddit;
@property (strong, nonatomic) NSString *pTitle;
@property (strong, nonatomic) NSString *pURL;
@property (strong, nonatomic) NSURL *pThumbnail;
@property (strong, nonatomic) NSMutableArray *pChildren;
@property (strong, nonatomic) NSString *pAuthor;
@property (strong, nonatomic) NSString *pBody;
@property (strong, nonatomic) NSString *pUps;
@property (nonatomic) int pLevel;

@property (nonatomic) BOOL pIsOpen;

@property ( nonatomic) BOOL pIsComment;


- (id)initValues;


@end
