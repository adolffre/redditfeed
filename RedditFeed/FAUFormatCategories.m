//
//  FAUFormatCategories.m
//  RedditFeed
//
//  Created by Adolf Jurgens on 05/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import "FAUFormatCategories.h"


@implementation NSDecimalNumber (FAUFormatCategories)

- (NSString *)toUIString
{
    if ([self integerValue] == 0)
        return @"0";
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.locale = [NSLocale currentLocale];
    numberFormatter.minimumFractionDigits = 2;
    numberFormatter.maximumFractionDigits = 0;
    return [numberFormatter stringFromNumber:self];
    
}


@end
