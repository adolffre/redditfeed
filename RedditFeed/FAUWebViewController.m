//
//  FAUWebViewController.m
//  RedditFeed
//
//  Created by Adolf Jurgens on 06/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import "FAUWebViewController.h"
#import "FAULibrary.h"

@interface FAUWebViewController ()

@end

@implementation FAUWebViewController{
        UIActivityIndicatorView *spinner;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = CGPointMake(160, 316);
    spinner.hidesWhenStopped = YES;
    [self.view addSubview:spinner];
    _pWebView.delegate = self;
    

    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:_pURL]];
    [_pWebView setBackgroundColor:[FAULibrary colorWithHexString:@"#e2e2e2"]];
    [NSThread detachNewThreadSelector: @selector(loading) toTarget:self withObject:nil];
    [_pWebView loadRequest:requestObj];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [spinner stopAnimating];

}

- (void)loading
{
    spinner.hidden = NO;
    [spinner startAnimating];
    
}


@end
