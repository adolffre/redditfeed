//
//  FAUPostTableViewCell.h
//  RedditFeed
//
//  Created by Adolf Jurgens on 05/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FAUPostTableViewCell;

@protocol FAUPostTableViewCellDelegate <NSObject>

- (void)didPressCommentsBtn:(FAUPostTableViewCell *)cell withIndex:(int )index;

@end

@interface FAUPostTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *pTitle;
@property (weak, nonatomic) IBOutlet UILabel *pScore;
@property (weak, nonatomic) IBOutlet UILabel *pSubReddit;
@property (weak, nonatomic) IBOutlet UILabel *pDomain;
@property (weak, nonatomic) IBOutlet UIImageView *pThumbnail;
@property (weak, nonatomic) IBOutlet UIButton *pBtnComments;
@property (weak, nonatomic) IBOutlet UIView *pContainerThumbnail;
@property (weak, nonatomic) id <FAUPostTableViewCellDelegate>delegate;

- (IBAction)aBtnComments:(id)sender;

@end
