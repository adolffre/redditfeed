//
//  FAUCommentsViewController.m
//  RedditFeed
//
//  Created by Adolf Jurgens on 07/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import "FAUCommentsViewController.h"
#import "UIImageView+WebCache.h"
#import "FAULibrary.h"
#import "KILabel.h"


#define kLineHeight 15
#define kMarginLeft 7
#define kMarginSon 7
#define kIconSons 99


@interface FAUCommentsViewController (){
    int offsetGeral;
    NSString* fontName;
    NSString* boldFontName;
    UIColor* mainColor;
    UIColor* mainColorLight;
    UIColor* neutralColor;
    NSString* urlSelected;
    
}

@end

@implementation FAUCommentsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self styleView];
    _pVisiblePosts =  [NSMutableArray new];
    [_pScrollGeral setBackgroundColor:[FAULibrary colorWithHexString:@"#e2e2e2"]];
    _pScrollArray  = [NSMutableArray new];
    _pScrollGeral = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, 320, 504)];
    [_pScrollGeral sizeToFit];
    [self.view addSubview:_pScrollGeral];


    
    [self createPost];

}
-(void)viewDidAppear:(BOOL)animated{
    [_pScrollGeral setContentSize:CGSizeMake(_pScrollGeral.frame.size.width ,
                                             offsetGeral)];
    
    _pScrollGeral.delegate = self;
    [_pScrollGeral setScrollEnabled:YES];
    
}
#pragma mark - Create view
-(void)createPost{
    
    UIScrollView *scrollPost = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    UILabel *lblAuthor = [[UILabel alloc] initWithFrame:CGRectMake(kMarginLeft, 1, 100, kLineHeight)];
    UILabel *lblSubReddit =[[UILabel alloc] initWithFrame:CGRectMake(87, 1, 76, kLineHeight)];
    UILabel *lblText =[[UILabel alloc] initWithFrame:CGRectMake(kMarginLeft, 16, 248, kLineHeight)];
    UILabel *lblDomainComments =[[UILabel alloc] initWithFrame:CGRectMake(kMarginLeft, 37, 248, kLineHeight)];
    UIView *containerThumbnail = [[UIView alloc] initWithFrame:CGRectMake(258, 1, 58, 58)];
    UIImageView *imgViewThumbnail =[[UIImageView alloc] initWithFrame:CGRectMake(4, 4, 50, 50)];
    [containerThumbnail addSubview:imgViewThumbnail];
    
    imgViewThumbnail.contentMode = UIViewContentModeScaleAspectFill;
    imgViewThumbnail.clipsToBounds = YES;
    imgViewThumbnail.layer.cornerRadius = 2.0f;
    
    containerThumbnail.backgroundColor = [UIColor whiteColor];
    containerThumbnail.layer.borderColor = [UIColor colorWithWhite:0.8f alpha:0.6f].CGColor;
    containerThumbnail.layer.borderWidth = 1.0f;
    containerThumbnail.layer.cornerRadius = 2.0f;

    lblText.textColor =  neutralColor;
    lblText.font =  [UIFont fontWithName:boldFontName size:10.0f];
    
    lblSubReddit.textColor = mainColorLight;
    lblSubReddit.font =  [UIFont fontWithName:fontName size:8.0f];
    
    lblDomainComments.textColor = mainColorLight;
    lblDomainComments.font =  [UIFont fontWithName:fontName size:8.0f];
    
    lblAuthor.textColor = mainColor;
    lblAuthor.font =  [UIFont fontWithName:fontName size:8.0f];
    
    [scrollPost setBackgroundColor:[UIColor whiteColor]];
    
    [lblAuthor setText:_pSelectedPost.pAuthor];
    [lblDomainComments setText:[NSString stringWithFormat:@"%@ - Comments(%i)",_pSelectedPost.pDomain, [_pSelectedPost.pNumComments intValue]]];
    [lblSubReddit setText:_pSelectedPost.pSubReddit];
    [lblText setText:_pSelectedPost.pTitle];
    
    NSString *file = [_pSelectedPost pURL];
    CFStringRef fileExtension = (__bridge CFStringRef) [file pathExtension];
    NSArray *imageExtensions = @[@"png", @"jpg", @"gif"];
    
    if ([imageExtensions containsObject:(__bridge id)(fileExtension)]) {
        [imgViewThumbnail setImageWithURL:[NSURL URLWithString:[_pSelectedPost pURL]] placeholderImage:[UIImage imageNamed:@"article-icon"]];
        UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc] initWithTarget : self action: @selector(imageTap:)] ;
        [containerThumbnail addGestureRecognizer : tapImage];

    }else{
        [imgViewThumbnail setImageWithURL:[_pSelectedPost pThumbnail] placeholderImage:[UIImage imageNamed:@"article-icon"]];
    }

    [lblText setNumberOfLines:0];
    [lblText sizeToFit];
    
    int offset = lblText.frame.size.height - kLineHeight;
    
    [lblDomainComments setFrame:CGRectMake(lblDomainComments.frame.origin.x,(lblDomainComments.frame.origin.y+offset), _pDomainCommentsPost.frame.size.width, kLineHeight)];
    
    int limit = lblDomainComments.frame.origin.y + lblDomainComments.frame.size.height +9 ;
    
    [lblDomainComments setFrame:CGRectMake(lblDomainComments.frame.origin.x, limit - 18, lblDomainComments.frame.size.width, lblDomainComments.frame.size.height)];
    [scrollPost setFrame:CGRectMake(scrollPost.frame.origin.x,scrollPost.frame.origin.y , scrollPost.frame.size.width, limit)];
    
    [scrollPost addSubview:lblAuthor];
    [scrollPost addSubview:lblSubReddit];
    [scrollPost addSubview:lblText];
    [scrollPost addSubview:lblDomainComments];
    [scrollPost addSubview:containerThumbnail];
    scrollPost.layer.cornerRadius = 4;
    scrollPost.layer.masksToBounds = YES;
    
    [_pScrollGeral addSubview:scrollPost];
    [_pScrollArray addObject:scrollPost];
    [_pVisiblePosts addObject:_pSelectedPost];
    offsetGeral += scrollPost.frame.size.height + 2;
    for (int i =0 ; i<_pSelectedPost.pChildren.count-1; i++) {
        [[_pSelectedPost.pChildren objectAtIndex:i] setPIsOpen:NO];
        [self createCommentsFromPost:[_pSelectedPost.pChildren objectAtIndex:i] andIndex:i];
        
    }
}
-(void)createCommentsFromPost:(FAUPost *)post andIndex:(int )index {
    [post setPLevel:0];
    [_pVisiblePosts insertObject:post atIndex:index+1];
    int offset, limit, heightCell, lastHeight;

    UIScrollView *lastScroll =[_pScrollArray objectAtIndex:index];
    
    UIScrollView *scrollComment = [[UIScrollView alloc]initWithFrame:CGRectMake(3, 0, 317, 480)];
    UIScrollView *contentTap = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 30, 20)];
    [contentTap setBackgroundColor:[UIColor clearColor]];
    
    UILabel *lblAuthor = [[UILabel alloc] initWithFrame:CGRectMake(15, 1, 200, 12)];
    UIImageView *imgSons = [[UIImageView alloc] initWithFrame:CGRectMake(3, 1, 13, 13)];
    [imgSons setTag:kIconSons];
    if(post.pChildren.count>1){
        UIImage *img = [UIImage imageNamed:@"arrow-right"];
        [imgSons setImage:img];
        [imgSons setContentMode:UIViewContentModeScaleAspectFit];
        contentTap.userInteractionEnabled = YES;
    }
    UILabel *lblUps = [[UILabel alloc] initWithFrame:CGRectMake(kMarginLeft, 1, 320 - 14, 10)];
    KILabel *lblText =[[KILabel alloc] initWithFrame:CGRectMake(5, 12, 313, 15)];
        lblText.automaticLinkDetectionEnabled = YES;

    [lblText becomeFirstResponder];
    lblText.linkTapHandler = ^(KILinkType linkType, NSString *string, NSRange range) {
        if (linkType == KILinkTypeURL)
        {
            urlSelected = string;
            _pSelectedPost = nil;
            [self performSegueWithIdentifier:@"idWebBrowser" sender:self];
        }
    };

    lblText.textColor =  neutralColor;
    lblText.font =  [UIFont fontWithName:boldFontName size:10.0f];
    
    lblAuthor.textColor = mainColor;
    lblAuthor.font =  [UIFont fontWithName:fontName size:9.0f];
    
    lblUps.textColor = neutralColor;
    lblUps.font =  [UIFont fontWithName:fontName size:9.0f];
    
    [scrollComment setBackgroundColor:[UIColor whiteColor]];
    [scrollComment setTag:index+1];

    [lblUps setTextAlignment:NSTextAlignmentRight];
    
    [lblAuthor setText:post.pAuthor];
    [lblUps setText:[NSString stringWithFormat:@"+%i",[post.pUps intValue]]];
    [lblText setText:post.pBody];
    [lblText setNumberOfLines:0];
    [lblText sizeToFit];
    
    offset = lblText.frame.size.height - kLineHeight;
    limit =  lblText.frame.origin.y + lblText.frame.size.height +4 ;
  
    
    lastHeight =lastScroll.frame.size.height +lastScroll.frame.origin.y;
    [scrollComment setFrame:CGRectMake(scrollComment.frame.origin.x,lastHeight +1, scrollComment.frame.size.width, limit)];
    
    [scrollComment addSubview:lblAuthor];
    [scrollComment addSubview:lblUps];
    [scrollComment addSubview:lblText];
    [scrollComment addSubview:imgSons];

    scrollComment.layer.cornerRadius = 4;
    scrollComment.layer.masksToBounds = YES;
    UITapGestureRecognizer *tapComments = [[UITapGestureRecognizer alloc] initWithTarget : self action: @selector(commentHasTap:)] ;
    [contentTap addGestureRecognizer : tapComments];

    [scrollComment addSubview:contentTap];
    
    heightCell = scrollComment.frame.size.height;
    
    
    [_pScrollGeral setContentSize:CGSizeMake(_pScrollGeral.frame.size.width, _pScrollGeral.frame.size.height + heightCell)];
    
    [_pScrollGeral addSubview:scrollComment];
    [_pScrollArray addObject:scrollComment];
    offsetGeral += scrollComment.frame.size.height + 1;
}
#pragma mark - Action Comments
-(void)openCommentsFromFather:(FAUPost *)post inScrollView:(UIScrollView *)scrollPost{
    
    for (UIView *view in scrollPost.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            if(view.tag  == kIconSons){
                UIImage *img = [UIImage imageNamed:@"arrow-down"];
                [(UIImageView *)view setImage:img];
            }
        }
    }

    
    int width = scrollPost.frame.size.width-kMarginSon;
    int xOrigin = scrollPost.frame.origin.x+kMarginSon;
    int yOrigin = scrollPost.frame.size.height+1;
    int offset;
    int limit,heightCell;
    int index =1;
    for (FAUPost *postChild in post.pChildren) {
        if(postChild.pBody){
            UIScrollView *scrollComment = [[UIScrollView alloc]initWithFrame:CGRectMake(xOrigin, yOrigin, width, 480)];
            UILabel *lblAuthor = [[UILabel alloc] initWithFrame:CGRectMake(15, 1, 76, 10)];
            UILabel *lblUps = [[UILabel alloc] initWithFrame:CGRectMake(kMarginLeft, 1, width - 14, 10)];
            UILabel *lblText =[[UILabel alloc] initWithFrame:CGRectMake(5, 12, width -5 , 15)];
            UIScrollView *contentTap = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 30, 20)];
            [contentTap setBackgroundColor:[UIColor clearColor]];
            UIImageView *imgSons = [[UIImageView alloc] initWithFrame:CGRectMake(3, 1, 13, 13)];
            
            [imgSons setTag:kIconSons];
            if(postChild.pChildren.count>1){
                UIImage *img = [UIImage imageNamed:@"arrow-right"];
                [imgSons setImage:img];
                [imgSons setContentMode:UIViewContentModeScaleAspectFit];
                contentTap.userInteractionEnabled = YES;
            }
            lblText.textColor =  neutralColor;
            lblText.font =  [UIFont fontWithName:boldFontName size:10.0f];
            
            lblAuthor.textColor = mainColor;
            lblAuthor.font =  [UIFont fontWithName:fontName size:9.0f];
            
            lblUps.textColor = neutralColor;
            lblUps.font =  [UIFont fontWithName:fontName size:9.0f];
            
            [scrollComment setBackgroundColor:[UIColor whiteColor]];
            
            [lblUps setTextAlignment:NSTextAlignmentRight];
            [lblUps setText:[NSString stringWithFormat:@"+%i",[postChild.pUps intValue]]];
            
            [lblAuthor setText:postChild.pAuthor];
            [lblText setText:postChild.pBody];
            [lblText setNumberOfLines:0];
            [lblText sizeToFit];

            offset = lblText.frame.size.height - kLineHeight;
            limit =  lblText.frame.origin.y + lblText.frame.size.height +4 ;
            
            [scrollComment setFrame:CGRectMake(scrollComment.frame.origin.x,yOrigin , scrollComment.frame.size.width, limit)];
            [scrollComment addSubview:lblAuthor];
            [scrollComment addSubview:lblUps];
            [scrollComment addSubview:lblText];
            [scrollComment addSubview:imgSons];
            scrollComment.layer.cornerRadius = 4;
            scrollComment.layer.masksToBounds = YES;
            UITapGestureRecognizer *tapComments = [[UITapGestureRecognizer alloc] initWithTarget : self action: @selector(commentHasTap:)] ;
            [contentTap addGestureRecognizer : tapComments];
            
            
            [scrollComment addSubview:contentTap];
            
            heightCell = scrollComment.frame.size.height;

            [_pScrollGeral insertSubview:scrollComment atIndex:index + scrollPost.tag];
            [postChild setPLevel:post.pLevel+1];
            [_pVisiblePosts insertObject:postChild atIndex:index + scrollPost.tag];
            index++;
        }
    }
    
}
-(void)closeCommentsFromFather:(FAUPost *)post inScrollView:(UIScrollView *)scrollPost{
    
    int max = 0;
    int initial = [scrollPost tag];
    int lvlPai = [[_pVisiblePosts objectAtIndex:initial] pLevel];
    int i  =1;
    while (true) {
        if([[_pVisiblePosts objectAtIndex:initial+i] pLevel]>lvlPai){
            max++;
            i++;
        }
        else
            break;
    }
    for (int i =max; i>0; i--) {
        [[_pScrollGeral viewWithTag:initial+i] removeFromSuperview];
        [_pVisiblePosts removeObjectAtIndex:initial+i];
        
    }
    for (UIView *view in scrollPost.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            if(view.tag  == kIconSons){
                UIImage *img = [UIImage imageNamed:@"arrow-right"];
                [(UIImageView *)view setImage:img];
            }
        }
    }
}
#pragma mark - Custom Methods
-(void)styleView{
    fontName = @"EuphemiaUCAS";
    boldFontName = @"EuphemiaUCAS-Bold";
    mainColor = [UIColor colorWithRed:216.0/255 green:58.0/255 blue:58.0/255 alpha:1.0];
    mainColorLight = [mainColor colorWithAlphaComponent:0.7];
    neutralColor = [FAULibrary colorWithHexString:@"#626262"];//[UIColor colorWithWhite:0.7 alpha:1.0];
}
-(void)resizeScrollGeral{
    int lastHeight = 0;
    int index = 0;
    for (UIScrollView *scroll in _pScrollGeral.subviews) {
        [scroll setFrame:CGRectMake(scroll.frame.origin.x, lastHeight , scroll.frame.size.width, scroll.frame.size.height)];
        lastHeight += scroll.frame.size.height +1;
        [scroll setTag:index];
        index++;
    }

    [_pScrollGeral setContentSize:CGSizeMake(_pScrollGeral.frame.size.width ,
                                   lastHeight-50)];
    [_pScrollGeral reloadInputViews];
    
}
#pragma mark - UITapGestureRecognizer
-(void)imageTap:(UITapGestureRecognizer *)sender{
    _pSelectedPost = [_pVisiblePosts objectAtIndex:0];
     [self performSegueWithIdentifier:@"idWebBrowser" sender:self];
}
-(void)commentHasTap:(UITapGestureRecognizer *)sender{
    UIScrollView *scrollFather = (UIScrollView *)[sender.view superview];
    FAUPost * post = [_pVisiblePosts objectAtIndex:scrollFather.tag];
    if(![post pIsOpen] && post.pChildren.count>1){
        //Open Children from post
        [post setPIsOpen:YES];
        [self openCommentsFromFather:post inScrollView:scrollFather];
        [self resizeScrollGeral];
    }
    else if([post pIsOpen]){
        //Close Children from post
        [post setPIsOpen:NO];
        [self closeCommentsFromFather:post inScrollView:scrollFather];
        [self resizeScrollGeral];
    }
}



#pragma mark - Segue Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"idWebBrowser"]){
        _FAUWebVC = segue.destinationViewController;
        if (_pSelectedPost) {
            [_FAUWebVC setPURL:_pSelectedPost.pURL];
        }else
            [_FAUWebVC setPURL:urlSelected];
    }

}
@end
