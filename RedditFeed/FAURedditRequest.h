//
//  FAURedditRequest.h
//  RedditFeed
//
//  Created by Adolf Jurgens on 05/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FAUPost.h"

@protocol FAURedditRequestDelegate <NSObject>

- (void)didFinishToLoadPost:(NSMutableArray *)posts withLast:(NSString *)afterResquest;
- (void)didFinishToLoadCommentsFromPost:(FAUPost *)post;

@end
@interface FAURedditRequest : NSObject

@property (weak, nonatomic) id <FAURedditRequestDelegate> delegate;
@property (strong, nonatomic) NSMutableArray *pPosts;
@property (strong, nonatomic) NSString *pAfterRequest;



- (void)getPostsFrom:(NSString *)type withLast:(NSString *)afterRequest;
-(void)getCommentsFromId:(NSString *)postId;
+ (BOOL) isSelfLink:(NSString *) link;
@end
