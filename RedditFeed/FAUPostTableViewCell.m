//
//  FAUPostTableViewCell.m
//  RedditFeed
//
//  Created by Adolf Jurgens on 05/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import "FAUPostTableViewCell.h"
#import "FAUPost.h"
#import "FAULibrary.h"

@implementation FAUPostTableViewCell
-(void)awakeFromNib{
    NSString* fontName = @"EuphemiaUCAS";
    NSString* boldFontName = @"EuphemiaUCAS-Bold";
    UIColor* mainColor = [UIColor colorWithRed:216.0/255 green:58.0/255 blue:58.0/255 alpha:1.0];
    UIColor* mainColorLight = [mainColor colorWithAlphaComponent:0.7];
    
    UIColor* neutralColor = [FAULibrary colorWithHexString:@"#626262"];
    
    _pTitle.textColor =  neutralColor;
    _pTitle.font =  [UIFont fontWithName:boldFontName size:13.0f];
    
    [_pBtnComments setTitleColor:mainColorLight forState:UIControlStateNormal];
    _pBtnComments.titleLabel.font =  [UIFont fontWithName:fontName size:11.0f];
    
    _pSubReddit.textColor = mainColor;
    _pSubReddit.font =  [UIFont fontWithName:fontName size:10.0f];
    
    _pDomain.textColor = neutralColor;
    _pDomain.font =  [UIFont fontWithName:fontName size:10.0f];
    
    _pScore.textColor = mainColor;
    _pScore.font =  [UIFont fontWithName:fontName size:10.0f];
    
    _pThumbnail.contentMode = UIViewContentModeScaleAspectFill;
    _pThumbnail.clipsToBounds = YES;
    _pThumbnail.layer.cornerRadius = 2.0f;
    
    _pContainerThumbnail.backgroundColor = [UIColor whiteColor];
    _pContainerThumbnail.layer.borderColor = [UIColor colorWithWhite:0.8f alpha:0.6f].CGColor;
    _pContainerThumbnail.layer.borderWidth = 1.0f;
    _pContainerThumbnail.layer.cornerRadius = 2.0f;
    
    self.selectionStyle = UITableViewCellSelectionStyleGray;
    
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

- (IBAction)aBtnComments:(id)sender {
    
    FAUPostTableViewCell *cell = (FAUPostTableViewCell *)[[[sender superview] superview] superview];
    NSIndexPath *indexPath = [(UITableView *)[[cell superview] superview] indexPathForCell:cell];
    [self.delegate didPressCommentsBtn:self withIndex:indexPath.row];
    
}


@end
