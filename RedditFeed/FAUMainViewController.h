//
//  FAUMainViewController.h
//  RedditFeed
//
//  Created by Adolf Jurgens on 04/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import "FAUCommentsViewController.h"
#import "FAUPostTableViewCell.h"
#import "FAURedditRequest.h"
#import "FAUWebViewController.h"
#import <UIKit/UIKit.h>

@interface FAUMainViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, FAURedditRequestDelegate, FAUPostTableViewCellDelegate>
@property (strong, nonatomic)  FAUCommentsViewController *FAUCommentsVC;
@property (strong, nonatomic)  FAUPost *pSelectedComment;
@property (strong, nonatomic)  FAUPost *pSelectedPost;
@property (strong, nonatomic)  FAUWebViewController *FAUWebVC;
@property (strong, nonatomic)  NSMutableArray *pPosts;
@property (strong, nonatomic)  NSString *pAfterRequest;
@property (weak, nonatomic) IBOutlet UITableView *pTableView;

@end
