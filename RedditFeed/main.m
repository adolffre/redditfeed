//
//  main.m
//  RedditFeed
//
//  Created by Adolf Jurgens on 04/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FAUAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FAUAppDelegate class]));
    }
}
