//
//  FAUMainViewController.m
//  RedditFeed
//
//  Created by Adolf Jurgens on 04/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import "FAUFormatCategories.h"
#import "FAULibrary.h"
#import "FAUMainViewController.h"
#import "FAUPostTableViewCell.h"
#import "FAURedditRequest.h"
#import "UIImageView+WebCache.h"



@interface FAUMainViewController ()


@end

@implementation FAUMainViewController{
    FAURedditRequest * request;
    UIActivityIndicatorView *spinner;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self styleView];
    _pPosts = [NSMutableArray new];
    request = [FAURedditRequest new];
    request.delegate = self;
    [request getPostsFrom:@"" withLast:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [_pTableView setBackgroundColor:[FAULibrary colorWithHexString:@"#e2e2e2"]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _pPosts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"idPostCell";
    
    NSInteger lastSectionIndex = [tableView numberOfSections] - 1;
    NSInteger lastRowIndex = [tableView numberOfRowsInSection:lastSectionIndex] - 1;
    if ((indexPath.row == lastRowIndex)) {
        [request getPostsFrom:@"" withLast:_pAfterRequest];
    }
    
    FAUPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil ) {
        cell = [[FAUPostTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.delegate = self;
    cell.pTitle.text = [[_pPosts objectAtIndex:indexPath.row] pTitle];
    cell.pScore.text = [[[_pPosts objectAtIndex:indexPath.row] pScore] stringValue];
    NSString *comments =[NSString stringWithFormat:@"Comments(%@)", [[[_pPosts objectAtIndex:indexPath.row] pNumComments] stringValue]];
    [cell.pBtnComments setTitle:comments forState:UIControlStateNormal];
    
    cell.pSubReddit.text = [[_pPosts objectAtIndex:indexPath.row] pSubReddit];
    cell.pDomain.text = [[_pPosts objectAtIndex:indexPath.row] pDomain];
    
    NSString *file = [[_pPosts objectAtIndex:indexPath.row] pURL];
    CFStringRef fileExtension = (__bridge CFStringRef) [file pathExtension];
    NSArray *imageExtensions = @[@"png", @"jpg", @"gif"];
    
    if ([imageExtensions containsObject:(__bridge id)(fileExtension)]) {
           [cell.pThumbnail setImageWithURL:[NSURL URLWithString:[[_pPosts objectAtIndex:indexPath.row] pURL]] placeholderImage:[UIImage imageNamed:@"article-icon"]];
    }else{
        [cell.pThumbnail setImageWithURL:[[_pPosts objectAtIndex:indexPath.row] pThumbnail] placeholderImage:[UIImage imageNamed:@"article-icon"]];
    }

    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FAUPost *post = [_pPosts objectAtIndex:indexPath.row];
    _pSelectedPost = post;
    if ([FAURedditRequest isSelfLink:post.pDomain])
    {
        [NSThread detachNewThreadSelector: @selector(loading) toTarget:self withObject:nil];
        [request getCommentsFromId:_pSelectedPost.pIdPost];
    }
    else
    {
        [self performSegueWithIdentifier:@"idWebBrowser" sender:self];
    }
}
#pragma mark - FAUPostTableViewCellDelegate

- (void)didPressCommentsBtn:(FAUPostTableViewCell *)cell withIndex:(int)index{
    [cell becomeFirstResponder];
    NSIndexPath *path = [NSIndexPath indexPathForRow:index inSection:0];
    [_pTableView selectRowAtIndexPath:path animated:YES scrollPosition:UITableViewScrollPositionMiddle];

    FAUPost *post = [_pPosts objectAtIndex:index];
    [NSThread detachNewThreadSelector: @selector(loading) toTarget:self withObject:nil];
    [request getCommentsFromId:post.pIdPost];

    
}
- (void)didFinishToLoadCommentsFromPost:(FAUPost *)post{
    _pSelectedComment = post;
    [spinner stopAnimating];
    [self performSegueWithIdentifier:@"idCommentsVC" sender:self];

}

#pragma mark - Custom Methods

- (void)didFinishToLoadPost:(NSMutableArray *)newPosts withLast:(NSString *)afterResquest{
    if (_pPosts) {
        [_pPosts addObjectsFromArray:newPosts];
    }
    _pAfterRequest = afterResquest?afterResquest:_pAfterRequest;
    
    [_pTableView reloadData];
}

- (void)loading
{
    spinner.hidden = NO;
    [spinner startAnimating];
}
-(void)styleView{
    //[_pTableView setBackgroundColor:[FAULibrary colorWithHexString:@"#C4E6E6"]];
    NSString* navigationTitleFont = @"EuphemiaUCAS-Bold";
    UIColor* color = [UIColor colorWithRed:216.0/255 green:58.0/255 blue:58.0/255 alpha:1.0];
    
    self.navigationController.navigationBar.barTintColor = color;
    
    self.navigationController.navigationBar.titleTextAttributes =
    
    @{NSForegroundColorAttributeName : [UIColor whiteColor],
      NSFontAttributeName : [UIFont fontWithName:navigationTitleFont size:18.0f]
      };
    
    _pTableView.separatorColor = [UIColor colorWithWhite:0.9 alpha:0.6];
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = CGPointMake(160, 316);
    spinner.hidesWhenStopped = YES;
    
    [self.view addSubview:spinner];
    
}

#pragma mark - Segue Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"idWebBrowser"]){
        _FAUWebVC = segue.destinationViewController;
        [_FAUWebVC setPURL:_pSelectedPost.pURL];
    }
    else if([segue.identifier isEqualToString:@"idCommentsVC"]){
        _FAUCommentsVC = segue.destinationViewController;
        [_FAUCommentsVC setPSelectedPost:_pSelectedComment];
    }
}

@end
