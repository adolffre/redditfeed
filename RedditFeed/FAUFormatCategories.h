//
//  FAUFormatCategories.h
//  RedditFeed
//
//  Created by Adolf Jurgens on 05/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDecimalNumber (FAUFormatCategories)

- (NSString *)toUIString;

@end
