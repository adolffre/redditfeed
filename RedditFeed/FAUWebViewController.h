//
//  FAUWebViewController.h
//  RedditFeed
//
//  Created by Adolf Jurgens on 06/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAUWebViewController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *pWebView;
@property (strong, nonatomic) NSString *pURL;


@end
