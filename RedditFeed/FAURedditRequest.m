//
//  FAURedditRequest.m
//  RedditFeed
//
//  Created by Adolf Jurgens on 05/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import "FAURedditRequest.h"
#import "SBJSON.h"
#import "FAUPost.h"

@implementation FAURedditRequest


- (void)getPostsFrom:(NSString *)type withLast:(NSString *)afterRequest {

    NSString *url;
    if(afterRequest){
        url = [NSString stringWithFormat:@"http://www.reddit.com/%@/.json?limit=10&after=%@",type,afterRequest];
    }
    else{
        url = [NSString stringWithFormat:@"http://www.reddit.com/%@/.json?limit=10", type];
        
    }
    _pPosts = [NSMutableArray new];
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest
                                          returningResponse:&response
                                                      error:&error];

    if (error == nil)
    {
        SBJSON *parser = [[SBJSON alloc] init];
        
        NSString * responseString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];

        NSMutableDictionary *response = [parser objectWithString:responseString error:nil];
        _pAfterRequest = [[response objectForKey:@"data"] objectForKey:@"after"];
        for (NSDictionary * post_data in [[response objectForKey:@"data"] objectForKey:@"children"])
        {
            NSMutableDictionary * post = [post_data objectForKey:@"data"];
            FAUPost *postObject = [[FAUPost alloc] initValues];
            postObject.pIdPost = [post objectForKey:@"id"];
            postObject.pTitle = [post objectForKey:@"title"];
            postObject.pScore =[post  objectForKey:@"score"];
            postObject.pDomain = [post objectForKey:@"domain"];
            postObject.pAuthor = [post objectForKey:@"author"];
            postObject.pSubReddit = [post objectForKey:@"subreddit"];
            postObject.pURL = [post objectForKey:@"url"];
            
            postObject.pThumbnail = [[NSURL alloc] initWithString:[post valueForKey:@"thumbnail"]];
            
            
            if ([post objectForKey:@"selftext"] && [[post objectForKey:@"selftext"] length])
            {
                postObject.pSelfText  =[post valueForKey:@"selftext"];
            }
            postObject.pNumComments = [post objectForKey:@"num_comments"];
            

			[_pPosts addObject:postObject];
        }
        
        [self.delegate didFinishToLoadPost:_pPosts withLast:_pAfterRequest];
    }
    
}
+ (BOOL) isSelfLink:(NSString *) link
{
	if(
	   [link rangeOfString:@"self." options:NSCaseInsensitiveSearch].location != NSNotFound ||
	   [link rangeOfString:@"reddit.com/comments/" options:NSCaseInsensitiveSearch].location != NSNotFound
	   )
		return true;
	else
		return false;
}


-(void )loadFatherComments:(FAUPost *)father withChildren:(NSMutableDictionary *)_children{
    for (NSDictionary *comment_data in [[_children objectForKey:@"data"] objectForKey:@"children"]) {
        NSMutableDictionary * post = [comment_data objectForKey:@"data"];
        FAUPost *postSon = [[FAUPost alloc] initValues];
        [postSon setPIsComment:YES];
        postSon.pIdPost = [post objectForKey:@"id"];
        postSon.pUps = [post objectForKey:@"ups"];
        postSon.pAuthor = [post objectForKey:@"author"];
        postSon.pBody =[post  objectForKey:@"body"];
        
        //NSLog(@"filho extra %i",i);
        NSMutableDictionary *childrenSon =[post objectForKey:@"replies"];
        if([childrenSon isKindOfClass:[NSMutableDictionary class]]){
            //NSLog(@"item %i tem + extras filhos",i);
            [self loadFatherComments:postSon withChildren:childrenSon];
        }
        else{
            //NSLog(@"nao tem mais filhos " );
        }
        [father.pChildren addObject:postSon];
        
    }
}
-(void)getCommentsFromId:(NSString *)postId{
    FAUPost *postFather = [[FAUPost alloc] initValues];
    NSString *url = [NSString stringWithFormat:@"http://www.reddit.com/comments/%@/.json?sort=top",postId];
    
    
    NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest
                                          returningResponse:&response
                                                      error:&error];
    if (error == nil)
    {
        SBJSON *parser = [[SBJSON alloc] init];
        
        NSString * responseString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSMutableDictionary *response = [parser objectWithString:responseString error:nil];
         int i = 0;
        for (NSDictionary * post_data in response  )
        {//father(post) and sons(comments)
        
            if(i!=0){
                //comments
                for (NSDictionary *comment_data in [[post_data objectForKey:@"data"] objectForKey:@"children"]) {
                    NSMutableDictionary * post = [comment_data objectForKey:@"data"];
                    i++;
                    FAUPost *postSon = [[FAUPost alloc] initValues];
                    [postSon setPIsComment:YES];
                    postSon.pIdPost = [post objectForKey:@"id"];
                    postSon.pUps = [post objectForKey:@"ups"];
                    postSon.pAuthor = [post objectForKey:@"author"];
                    postSon.pBody =[post  objectForKey:@"body"];
                    
                    
                    
                    NSMutableDictionary *childrenSon =[post objectForKey:@"replies"];
                    if([childrenSon isKindOfClass:[NSMutableDictionary class]]){
                        //more comments on this comment
                        [self loadFatherComments:postSon withChildren:childrenSon];
                    }
                    [postFather.pChildren addObject:postSon];
                    
                }
            }
            else
            {//post
                for (NSDictionary *comment_data in [[post_data objectForKey:@"data"] objectForKey:@"children"]) {
                    NSMutableDictionary * post = [comment_data objectForKey:@"data"];
                    if (i==0) {
                        postFather.pIdPost = [post objectForKey:@"id"];
                        postFather.pTitle = [post objectForKey:@"title"];
                        postFather.pScore =[post  objectForKey:@"score"];
                        postFather.pDomain = [post objectForKey:@"domain"];
                        postFather.pSubReddit = [post objectForKey:@"subreddit"];
                        postFather.pURL = [post objectForKey:@"url"];
                        postFather.pAuthor = [post objectForKey:@"author"];
                        postFather.pThumbnail = [[NSURL alloc] initWithString:[post valueForKey:@"thumbnail"]];
                        postFather.pNumComments = [post objectForKey:@"num_comments"];
                        
                        i++;
                    }
                }
            }
            
        }
        
    }
    [self.delegate didFinishToLoadCommentsFromPost:postFather ];

}

@end
