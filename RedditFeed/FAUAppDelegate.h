//
//  FAUAppDelegate.h
//  RedditFeed
//
//  Created by Adolf Jurgens on 04/07/14.
//  Copyright (c) 2014 Adolf Jurgens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
